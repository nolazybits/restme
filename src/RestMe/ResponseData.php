<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 21/05/14
 * Time: 11:54 AM
 */

namespace RestMe;

/**
 * A formatted result object returned to the client from a REST request
 */
class ResponseData
{
    /**
     * Response code
     * @var string
     */
    public $code;

    /**
     * Data object to be parsed out.
     * If an error, this will provide more details about the error that occurred, if available.
     * @var mixed
     */
    public $data;


    /**
     * Message
     * If an error, this will provide more details about the error that occurred, if available.
     * @var mixed
     */
    public $message;

    /**
     *
     */
    public function __construct()
    {
        $this->clear();
    }

    /**
     *
     */
    public function clear()
    {
        $this->code = 0;
        $this->message = null;
        $this->data = null;
    }
}
?>
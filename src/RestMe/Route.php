<?php
/**
 * User: nolazybits
 * Date: 9/04/14
 * Time: 12:22 PM
 */

namespace RestMe;


class Route
    extends \RestMe\Options
{
    /**
     * @var \string
     */
    private $httpMethod;

    /**
     * @var \string
     */
    private $httpURI;

    /**
     * The controller or method this map points to
     * @var \string;
     */
    private $class;

    /**
     * @param \string $httpMethod  http method
     * @param \string $httpURI     Resource URI
     * @param \string $class       class and or function qualifying string
     * @param \array $options      supported options for this resource. used to override server defaults
     *
     * <li>\array $mimeTypes    mimeTypes this route supports</li>
     * <li>\array $pre_modules   modules to be executed before the route logic</li>
     * <li>\array $post_modules  modules to be executed after the route logic</li>
     */
    public function __construct($httpMethod, $httpURI, $class, $options = null)
    {
        parent::__construct();
        $this->httpMethod = $httpMethod;
        $this->httpURI = $httpURI;
        $this->class = $class;

        if( !is_null($options) )
        {
            if(isset($options['mimeTypes']))
            {
                $this->addAcceptedMimeTypes($options['mimeTypes']);
            }

            if(isset($options['pre_modules']))
            {
                $this->addPreModules($options['pre_modules']);
            }

            if(isset($options['post_modules']))
            {
                $this->addPostModules($options['post_modules']);
            }
        }
    }

    /**
     * The http method this route reacts to
     * @return \string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * The http URI this route reacts to
     * @return \string
     */
    public function getHttpURI()
    {
        return $this->httpURI;
    }

    /**
     * The code logic to call when the route is run
     * @return \string
     */
    public function getClass()
    {
        return $this->class;
    }
}
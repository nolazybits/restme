<?php
/**
 * User: nolazybits
 * Date: 9/04/14
 * Time: 11:11 AM
 */

namespace RestMe;

class Server
    extends \RestMe\Options
{
    /**
     * @var \RestMe\Response
     */
    private $response;

    /**
     * @var \RestMe\Request
     */
    private $request;

    /**
     * @var string
     */
    private $baseURL;

    /**
     * @var null|string
     */
    private $query;

    /**
     * @var
     */
    private $params;

    /**
     * @var array [apiName=>[apiVersions]]
     */
    private $apis;

    /**
     * @var \RestMe\API The api being accessed
     */
    private $api;

    /**
     * @var \RestMe\Exceptions\BaseExceptionHandler
     */
    private $exceptionHandler;

    /**
     * The Mime type which will be used to return the data
     * @var \string
     */
    protected $returnMimeType;

    /**
     * The default Mime type which will be used to return the data, if none have been asked
     * @var \string
     * @default 'application/json'
     */
    protected $defaultReturnMimeType;

    /**
     * If true the apiname is before the version, if false, after.
     * @var bool
     * @default true
     */
    protected $apiNameBeforeVersion = true;

    /**
     * @private
     * @var array the server call stack
     */
    private $stack;

    /**
     * @param \string $query Optional query to be treat as the URL
     * @throws Exceptions\Error\Exception500InternalServerError
     */
    public function __construct($query = null)
    {
        parent::__construct();

    //  default exception handler
        $this->exceptionHandler = new \RestMe\Exceptions\ExceptionHandler($this);

    //  Request handler
        $this->request = new Request($this);

    //  Response holder
        $this->response = new Response($this);

        $this->defaultReturnMimeType = \RestMe\Http\MimeType::JSON;

    //  mimeTypes the server handles
        $this->extensions = array();

    //  set the base url
        if(isset($_SERVER["HTTP_HOST"]))
        {
            if( (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
                || (!empty( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
            {
                $this->baseURL = "https://";
            }
            else
            {
                $this->baseURL = "http://";
            }
            $this->baseURL .= $_SERVER["HTTP_HOST"].dirname($_SERVER["SCRIPT_NAME"]);
        }

    //  If will use custom URI or HTTP requested URI
        if($query===null)
        {
            $this->query = $this->getRequest()->getURI() ;
        }
        else
        {
            $this->query = $query ;
        }

    //  Set the query on the Request
        $this->getRequest()->setURI($this->query);

        $this->apis = [];
    }

    /**
     * @param \RestMe\API $api
     * @throws Exceptions\Error\Exception500InternalServerError
     */
    public function addAPI($api)
    {
        //  get the api name and version
        $apiName    = $api->getName();
        $apiVersion = $api->getVersion();

        // find if we have added an api with the same name, if not create a new record
        if( !isset($this->apis[$apiName]) )
        {
            $this->apis[$apiName] = [];
        }

        //  find if the api number is already registered with this api
        if (isset($this->apis[$apiName][$apiVersion]))
        {
            throw new \RestMe\Exceptions\Error\Exception500InternalServerError("The api $apiName $apiVersion " .
                "has already been registered with the server");
        }
        else
        {
            $this->apis[$apiName][$apiVersion] = $api;
        }
    }

    /**
     * @param \RestMe\API $api
     */
    public function removeAPI($api)
    {
        $apiVersion = $api->getVersion();
        $apiName    = $api->getName();

        // clean the arrays
        if( $this->apis[$apiName] && $this->apis[$apiName][$apiVersion] )
        {
            unset ($this->apis[$apiName][$apiVersion]);
            //  remove the api name if no more version exists for this api
            if( $this->apis[$apiName] && count($this->apis[$apiName]) == 0)
            {
                unset($this->apis[$apiName]);
            }
        }
    }

    /**
     * @return \RestMe\API the api being accessed
     */
    public function getAPI()
    {
        return $this->api;
    }

    /**
     * Run the Server to handle the request and prepare the response
     * @throws Exceptions\Error\Exception404NotFound
     */
    public function run($echo=true)
    {
    //  execute pre modules
        $this->executeModules( $this->getPreModules() );

    //  get the different parts
        $uriParts = $this->request->getURIParts();

        //  find api name and version number (v1, v2, v2.2, ...)
        $max = count($uriParts)-1;
        for ($i=0; $i<=$max; $i++)
        {
            if( !empty($uriParts[$i]) && strcasecmp($uriParts[$i][0], 'v') == 0 && preg_match("/^v\d+.*$/i", $uriParts[$i]) == 1 )
            {
                if( $this->apiNameBeforeVersion)
                {
                    $apiName = implode( '/', array_slice($uriParts, 1, $i-1) );
                }
                else
                {
                    $apiName = $uriParts[$i+1];
                }
                $apiVersion = $uriParts[$i];

                // check the api name has been registered with the server
                if ( !isset($this->apis[$apiName]) )
                {
                    throw new \RestMe\Exceptions\Error\Exception404NotFound("This API name '$apiName' doesn't exists.");
                }

                //  check the api version for this api has been registered with the server
                if( !isset($this->apis[$apiName][$apiVersion]) )
                {
                    throw new \RestMe\Exceptions\Error\Exception404NotFound("This API version doesn't exists.");
                }

                //  set the acccessed api
                $this->api = $this->apis[$apiName][$apiVersion];
                if( $this->apiNameBeforeVersion)
                {
                    $this->getRequest()->setRouteURI("/" . implode("/", array_slice($uriParts, $i+1, $max-(strlen($this->getRequest()->getExtension()))-1)));
                }
                else
                {
                    $this->getRequest()->setRouteURI("/" . implode("/", array_slice($uriParts, $i+2, $max-(strlen($this->getRequest()->getExtension()))-1)));
                }

                // run the api
                $this->api->run($this);
            }
        }

        // execute the server post modules
        $this->executeModules( $this->getPostModules() );

        $this->echoResponse($echo);

        return $this;
    }

    public function echoResponse($echo=true)
    {
        //  echo the response + etag
        $r = $this->getResponse()->getData();
        $doEcho = $echo && strlen($r) >= 1;
        if( $doEcho )
        {
            //  TODO, need to add a hook to get and check ETag
            if ($this->getRequest()->getMethod() == \RestMe\Http\Methods::GET || $this->getRequest()->getMethod() == \RestMe\Http\Methods::HEAD)
            {
                $this->getResponse()->addheader(\RestMe\Http\HeaderConstants::ETAG . ": " . md5($r));
            }
        }

        // Call headers, if no yet
        if( !$this->getResponse()->headerSent() )
        {
            $this->getResponse()->showHeader();
        }

        //  echo the response if any
        if( $echo && strlen($r) >= 1 )
        {
            echo $r;
        }
    }

    /**
     * Loop in the list of Modules (implement Action) and execute
     * @param \RestMe\Action[] $modules
     */
    public function executeModules( $modules )
    {
        foreach($modules as $module)
        {
            $this->executeAction($module);
        }
    }

    /**
     * @param mixed $class
     * @return $this
     */
    public function executeAction( $class = null )
    {
        //  TODO fully automatic routing, using URI path and http method

        //  the class is actually an anonymous function, add it to the Generic Controller
        if( !is_string($class) && is_callable($class) )
        {
            $object = new \RestMe\Generics\Action( $class );
            $method = "execute";
        }

        //  we have passed either class::method or class
        else if( is_string($class) )
        {
            //  we have both the class name and method name, easy.
            $response = explode("::",$class);
            if ( count($response) == 2)
            {
                $object =  new $response[0]($this);
                $method = $response[1];
            }
            //  we have only the class name, the method name is the http method used
            else
            {
                $object = new $response[0]($this);
                $method = strtolower($this->getRequest()->getMethod());
            }
        }

        $this->call($object,$method);

        return $this;
    }

    private function call($object,$method)
    {
        $this->stack[] = get_class($object) ;
        if( !isset($object) || !($object instanceof \RestMe\Action) )
        {
            throw new \RestMe\Exceptions\Error\Exception500InternalServerError( get_class($object)." is not a \\RestMe\\Action" );
        }
        else
        {
            //  the method define in the route cannot be called, return a 500
            if( !method_exists($object, $method) || !is_callable(array($object, $method)) )
            {
                throw new \RestMe\Exceptions\Error\Exception404NotFound("The method for this resource is not callable.");
            }

            $class = $object->$method($this);
        }

        if( $class instanceof \RestMe\Action && get_class($class) != $this->lastClass() )
        {
            return $this->call($class,"execute"); // May have another class to follow the request
        }
        return $this;
    }

    /**
     * Return last class name from RestServer stack trace
     * @return string
     */
    public function lastClass()
    {
        $i = count($this->stack);
        return $this->stack[$i - 1];
    }

    /**
     * Sets a parameter in a global scope that can be recovered at any request.
     * @param mixed $key The identifier of the parameter
     * @param mixed $value The content of the parameter
     * @return \RestMe\Server
     */
    public function setParameter($key,$value)
    {
        $this->params[$key] = $value ;
        return $this ;
    }

    /**
     * Return all parameters
     * @return mixed
     */
    public function getParameters()
    {
        return $this->params;
    }

    /**
     * Return the specified parameter
     * @param mixed $key The parameter identifier
     * @return mixed
     */
    public function getParameter($key)
    {
        return $this->params[$key];
    }

    /**
     * Set the URL to be handle or part of it
     * @param mixed $value The url
     * @return \RestMe\Server
     */
    public function setQuery($value)
    {
        $this->getRequest()->setURI($value);
        return $this ;
    }

    /**
     * Get the URL or part of it, depreciated by RestRequest::getURI();
     * @param \string $k uri part
     * @return \string
     **/
    public function getQuery($k=null)
    {
        return $this->getRequest()->getURI($k);
    }

    /**
     * Get the baseurl, based on website location (eg. localhost/website or website.com/);
     * @return string
     **/
    public function getBaseURL()
    {
        return $this->baseURL ;
    }

    /**
     * Get the Response handler object
     * @return \RestMe\Response
     */
    public function getResponse()
    {
        return $this->response ;
    }

    /**
     * Get the Request handler object
     * @return \RestMe\Request
     */
    public function getRequest()
    {
        return $this->request ;
    }

    /**
     * The mime type we have been ask to return
     * @return \string
     */
    public function getReturnMimeType()
    {
        return $this-> returnMimeType;
    }

    /**
     * @param $type
     * @return \string
     */
    public function setReturnMimeType($type)
    {
        $this-> returnMimeType = $type;
    }

    /**
     * The default mime type the server returns if none have been specified
     * @return \string
     */
    public function getDefaultReturnMimeType()
    {
        return $this-> defaultReturnMimeType;
    }
    public function setDefaultReturnMimeType($type)
    {
        $this->defaultReturnMimeType = $type;
    }

    /**
     * @return bool
     */
    public function getIsAPINameBeforeVersion()
    {
        return $this->apiNameBeforeVersion;
    }

    /**
     * @param $bool
     */
    public function setIsApiNameBeforeVersion($bool)
    {
        $this->apiNameBeforeVersion = $bool;
    }

    /**
     * @return Exceptions\BaseExceptionHandler
     */
    public function getExceptionHandler()
    {
        return $this->exceptionHandler;
    }

    /**
     * @param \RestMe\Exceptions\BaseExceptionHandler $exceptionHandler
     */
    public function setExceptionHandler( $exceptionHandler )
    {
        $this->exceptionHandler = $exceptionHandler;
    }
}
<?php
/**
 * User: nolazybits
 * Date: 20/03/13
 * Time: 4:50 PM
 */

namespace RestMe\Http;

class Methods
{
    const POST      = "POST";
    const GET       = "GET";
    const PUT       = "PUT";
    const HEAD      = "HEAD";
    const DELETE    = "DELETE";
    const OPTIONS   = "OPTIONS";
}

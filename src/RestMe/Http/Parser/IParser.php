<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 17/10/14
 * Time: 11:06 AM
 */

namespace RestMe\Http\Parser;

interface IParser
{
    /**
     * Parse the string
     * @param \string $string
     * @return mixed
     */
    static public function parse($string);
}
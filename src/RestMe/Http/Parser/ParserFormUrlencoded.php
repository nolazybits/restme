<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 17/10/14
 * Time: 11:06 AM
 */

namespace RestMe\Http\Parser;

class ParserFormUrlencoded
    implements IParser
{
    /**
     * Parse the string
     * @param \string $string
     * @return array
     */
    static public function parse($string)
    {
        $result = [];
        $exploded = explode('&', $string);
        foreach($exploded as $pair)
        {
            $item = explode('=', $pair);
            if(count($item) == 2)
            {
                $result[urldecode($item[0])] = urldecode($item[1]);
            }
        }
        return $result;
    }
}
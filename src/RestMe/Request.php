<?php

/**
* User: nolazybits
* Date: 9/04/14
* Time: 12:22 PM
*/

namespace RestMe;

/**
 * Holds the Request in a RestMe Server
 */

class Request
{

    private $server ;

    private $requestURI ;
    private $URIParts ;

    private $requestMethod ;
    private $get ;
    private $post ;
    private $put ;
    private $files ;
    private $headers ;
    private $routeURI;
    private $mimeTypes;
    private $contentType;
    private $charset;
    /**
     * the extension provided in the URI. It will override any Accept mime type set in the header.
     * @var \string
     */
    private $extension;

    /**
     * @param \RestMe\Server $server
     */
    public function __construct(Server $server=null)
    {

        // Sets most of the parameters
        $this->server = $server ;

        if(isset($_SERVER["REQUEST_METHOD"]))
        {
            $this->requestMethod = $_SERVER["REQUEST_METHOD"];
        }
        if(isset($_SERVER["REDIRECT_URL"]))
        {
            $this->requestURI = $_SERVER["REDIRECT_URL"];
        }
        else if(isset($_SERVER["REQUEST_URI"]))
        {
            $this->requestURI = $_SERVER["REQUEST_URI"];
        }
        $this->URIParts = explode("/",$this->requestURI);

        $this->mimeTypes = new \RestMe\MimeTypes();

        //  check for extension in the URI
        $pathInfo = pathinfo($this->getURI());
        if( isset($pathInfo['extension']))
        {
            $this->extension = $pathInfo['extension'];
        }

        /*$reg = array();
        preg_match('@\.([a-zA-Z0-9]{1,5})$@',$this->getURI(),$reg);
        if(isset($reg[1]))
        {
            $this->extension = $reg[1];
        }*/

        $this->domain = $_SERVER["HTTP_HOST"];
        //  TODO check for https
        $this->URL = "http://".$this->domain.$this->requestURI;

        $this->headers = [];
        //  we are using our own function instead of getallheaders (specific to apache)
        //  see http://www.php.net/manual/en/function.getallheaders.php
        foreach($_SERVER as $name=>$value)
        {
            if(substr($name,0,5) == "HTTP_")
            {
                $hName = substr($name,5);
                $hName = strtolower(str_replace("_",'-',$hName));
                $this->headers[$hName] = $value;
            }
            else if ($name == "CONTENT_TYPE" || $name == "CONTENT_LENGTH")
            {
                $hName = strtolower(str_replace("_",'-',$name));
                $this->headers[$hName] = $value;
                $exploded = explode(";", $value, 2);
                $this->contentType = $exploded[0];
                if( isset($exploded[1]) && strpos($exploded[1], '='))
                {
                    $this->charset = strtolower(explode('=',$exploded[1])[1]);
                }
            }
        }

        $this->get = $_GET?$_GET:[];
        $this->post = $_POST?$_POST:[];
        $this->files = $_FILES?$_FILES:[] ;
        $this->put = file_get_contents('php://input');
    }

    /**
     * Return  \RestMe\Server used
     * @return \RestMe\Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Returns if Request is GET
     * @return boolean
     */
    public function isGet()
    {
        if($this->requestMethod == "GET")
        {
            return true ;
        }
        return false;
    }

    /**
     * Returns if Request is POST
     * @return boolean
     */
    public function isPost()
    {
        if($this->requestMethod == "POST")
        {
            return true ;
        }
        return false;
    }

    /**
     * Return if Request is PUT
     * @return boolean
     */
    public function isPut()
    {
        if($this->requestMethod == "PUT")
        {
            return true ;
        }
        return false;
    }

    /**
     * Return true if Request is DELETE
     * @return boolean
     */
    public function isDelete()
    {
        if($this->requestMethod == "DELETE")
        {
            return true ;
        }
        return false;
    }


    /**
     * Get parameters sent with GET (url parameters)
     * @param mixed $k get[$key]
     * @return mixed
     */
    public function getGet($k=null)
    {
        if($k==null)
        {
            return $this->get ;
        }
        elseif (isset($this->get[$k]))
        {
            return $this->get[$k];
        }
        return null;
    }

    /**
     * Set the $_GET array that will be used by the server
     * @param array $get
     */
    public function setGet($get)
    {
        if( is_null($get))
        {
            $get = [];
        }
        $this->get = $get;
    }

    /**
     * Return parameters sent on a POST
     * @param mixed $k post[$key]
     * @return mixed
     */
    public function getPost($k=null)
    {
        if($k==null)
        {
            return $this->post ;
        }
        elseif (isset($this->post[$k]))
        {
            return $this->post[$k];
        }
        return null;
    }

    /**
     * Set the $_POST array that will be used by the server
     * @param array $post
     */
    public function setPost($post)
    {
        if( is_null($post))
        {
            $post = [];
        }
        $this->post = $post;
    }

    /**
     * Return FILES sent on a POST
     * @param mixed $k file[$key]
     * @return mixed
     */
    public function getFiles($k=null)
    {
        if($k==null)
        {
            return $this->files ;
        }
        return $this->files[$k];
    }

    /**
     * Set the $_FILES array that will be used by the server
     * @param array $files
     */
    public function setFiles($files)
    {
        if( is_null($files))
        {
            $files = [];
        }
        $this->files = $files;
    }

    /**
     * Return content sent with PUT
     * @param mixed $k
     * @return mixed
     */
    public function getPut($k=null)
    {
        if($k==null)
        {
            return $this->put ;
        }
        return $this->put[$k];
    }

    /**
     * Set the Request Payload (put) array that will be used by the server
     * @param array $put
     */
    public function setPut($put)
    {
        if( is_null($put))
        {
            $put = [];
        }
        $this->put = $put;
    }

    /**
     * Return content sent with PUT
     * @return mixed
     */
    public function getInput()
    {
        return file_get_contents('php://input');
    }

    /**
     * Return request BODY
     * @return string
     */
    public function getBody()
    {
        return $this->getInput();
    }

    /**
     * Return Request Method(PUT, DELETE, OPTION, GET...)
     * @return string
     */
    public function getMethod()
    {
        return $this->requestMethod ;
    }

    /**
     * Return value of http header
     * @param string $k
     * @return string
     */
    public function getHeader($k)
    {
        if(isset($this->headers[strtolower($k)]))
        {
            return $this->headers[strtolower($k)];
        }
        return null;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return \string E-tag/if-match requested
     */
    public function getETag()
    {
        return $this->getheader("If-Match");
    }

    /**
     * Set the request method
     * @param string $method the request method
     * @return \RestMe\Request RestMe server
     */
    public function setMethod($method)
    {
        $this->requestMethod = $method ;
        return $this;
    }

    /**
     * check if a parameter exist
     * @param $parameter
     * @return bool
     */
    public function hasParameter($parameter)
    {
        //  find parameter by name
        $routeURI = $this->getServer()->getAPI()->getRoute();
        $httpURI = explode('/', $routeURI->getHttpURI());

        foreach($httpURI as $index => $part)
        {
            if ($part == ":" . $parameter || $part == ":?" . $parameter)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Used to access parameter like :name or :?name
     * @param \string parameter name
     * @return \string|null parameter value
     */
    public function getParameter($parameter)
    {
        //  find parameter by name
        $routeURI = $this->getServer()->getAPI()->getRoute();
        $httpURI = explode('/', $routeURI->getHttpURI());

        foreach($httpURI as $index => $part)
        {
            if($part == ":".$parameter || $part == ":?".$parameter)
            {
                return explode('/', $this->getRouteURI())[$index];
            }
        }
        return null;
    }

    /**
     * Set the parameter
     * @param \string $name
     * @param \string $value
     */
    public function replaceParameter($name, $value)
    {
        //  find parameter by name
        $routeURI = $this->getServer()->getAPI()->getRoute();
        $httpURI = explode('/', $routeURI->getHttpURI());

        foreach($httpURI as $index => $part)
        {
            if ($part == ":" . $name || $part == ":?" . $name)
            {
                $routeURI = explode('/', $this->getRouteURI());
                $routeURI[$index] = $value;
                $this-> setRouteURI(implode('/', $routeURI));
            }
        }
    }

    /**
     * Return an associative array with key parameter name and value parameter value
     * @return array An associative array key parameter name and value parameter value
     */
    public function getParameters()
    {
        $routeURI = $this->getServer()->getAPI()->getRoute();
        $httpURI = explode('/', $routeURI->getHttpURI());

        $parameters = [];
        foreach($httpURI as $index => $part)
        {
            preg_match('/^:\??(.*)/', $part, $matches);
            if( count($matches) > 1)
            {
                $parameters[$matches[1]] = explode('/', $this->getRouteURI())[$index];
            }
        }
        return $parameters;
    }

    /**
     * @param \int $i
     * @return \string|null
     */
    public function getURIPart($i)
    {
        if(is_int($i) && isset($this->URIParts[$i]))
        {
            return $this->URIParts[$i];
        }
        return null;
    }

    /**
     * @return \string[] Request URI parts ('/' exploded)
     */
    public function getURIParts()
    {
        return $this->URIParts;
    }

    /**
     * @return \string the URI requested
     */
    public function getURI()
    {
        return $this->requestURI ;
    }

    /**
     * Sets the URI to deal
     * @param string $uri
     * @return \RestMe\Request
     */
    public function setURI($uri)
    {
        $this->requestURI = $uri;
        $this->URIParts = explode("/",$this->requestURI);
        return $this ;
    }

    /**
     * Use internaly to set the route URI for the api for the requested resource
     * @param \string $routeURI
     */
    public function setRouteURI($routeURI)
    {
        $this->routeURI = $routeURI;
    }

    /**
     * @return \string|null the requested route URI (set by the server once valid API has been found)
     */
    public function getRouteURI()
    {
        return $this->routeURI;
    }

    /**
     *
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Return the Accept Mime Types (ordered as in string or via quantifier) pass in the http accept header or
     * the mime type set in the extension (override the accept header)
     * @return \array the types pass as extension or in http accept
     */
    public function acceptMime()
    {
        $mimeTypes = [];

        //**first check for extension, if any it will override what has been sent in the headers
        if($this->extension)
        {
            //  find the mime type from the extension.
            $mimeType = $this->mimeTypes->getMime($this->extension);
            if( is_null( $mimeType ) )
            {
                return "unknow with extension $this->extension";
            }
            else
            {
                $mimeTypes[] = $mimeType;
                return $mimeTypes;
            }
        }

        //**we haven't found any extension, check the headers
        //  we have to get the http_accept, explode on ",", explode on ";" for quantifier and return an indexed array
        $acceptedTypesString = $_SERVER["HTTP_ACCEPT"];
        if( is_null($acceptedTypesString) )
        {
            return "application/json";
            // text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
        }

        $acceptedTypes = explode(",", $acceptedTypesString);

        //  get all the types without quantifier and add them to the index.
        foreach($acceptedTypes as $key => $acceptedType)
        {
            if (strpos($acceptedType, "q=") === FALSE)
            {
                $mimeTypes[] = $acceptedType;
                unset($acceptedTypes[$key]);
            }
        }

        //  now sort it index 0 is the first asked mime type, 1 the second, ...
        $sortArray = function($a, $b)
        {
            $posaq = strpos($a, "q=");
            $posbq = strpos($b, "q=");

            $qa = (float) substr($a, $posaq+2);
            $qb = (float) substr($b, $posbq+2);
            if( $qa === $qb)
            {
                return 0;
            }
            return ($qa < $qb) ? 1 : -1;
        };
        usort($acceptedTypes, $sortArray);

        //  and strip the quantifier
        $stripQuantifier = function($mime)
        {
            return explode(";", $mime)[0];
        };
        $acceptedTypes = array_map($stripQuantifier, $acceptedTypes);

        return array_merge($mimeTypes, $acceptedTypes);
    }

    /**
     */
    public function getSession($k)
    {
        if(!isset($_SESSION[$k]))
        {
            return null;
        }
        return $_SESSION[$k];
    }

    /**
     */
    public function setSession($k,$v)
    {
        $_SESSION[$k] = $v;
        return $this;
    }

    /**
     */
    public function getCookie($k)
    {
        if(!isset($_COOKIE[$k]))
        {
            return null;
        }
        return $_COOKIE[$k];
    }

    /*
     */
    public function setCookie($k,$v)
    {
        $_COOKIE[$k] = $v;
        setcookie($k,$v);
    }

    public function getContentType()
    {

    }

    public function getCharset()
    {

    }
}

<?php
/**
 * User: nolazybits
 * Date: 9/04/14
 * Time: 3:19 PM
 */

namespace RestMe;

class API
    extends \RestMe\Options
{
    /**
     * represents the query string passed in the URI being accessed
     * @var \RestMe\Server
     */
    protected $server;

    /**
     * represents the api being accessed
     * @var string
     */
    protected $name;

    /**
     * represets the version of api being accessed
     * @var string
     */
    protected $version;

    /**
     * @var array of Route
     */
    private $routes;

    /**
     * The selected route
     * @var \RestMe\Route
     */
    private $route;

    /**
     *
     */
    public function __construct($name='', $version = 'v1')
    {
        parent::__construct();
        //  validate name
        if( preg_match("/^([!#$&-;=?-\[\]_a-z~]|%[0-9a-fA-F]{2})+$/", $version) == 0 )
        {
            throw new \RestMe\Exceptions\Error\Exception500InternalServerError("The specified name for the API has an invalid format. It should follow http://tools.ietf.org/html/rfc3986#section-2 or regex ^([!#$&-;=?-\[\]_a-z~]|%[0-9a-fA-F]{2})+$");
        }

        //  validate version
        if( strcasecmp($version[0], 'v') != 0 || preg_match("/^v\d+.*$/i", $version) == 0 )
        {
            throw new \RestMe\Exceptions\Error\Exception500InternalServerError("The specified version for the API has an invalid format. It should follow ^vd+.*$ ");
        }

        $this->version = $version;
        $this->name = $name;
        $this->routes = [];
    }

    /**
     * Map a HTTP method and URI to a controller or anonymous function
     * <br />
     * Automatic, Semi automatic and manual mapping can be done. Please see the $class parameter
     * @param string|string[] $httpMethods the http method used to access the uri
     * @param string $httpURI the URI accessed uri
     * @param null|string $class what to execute for this uri/method<br/>
     * <li>if not set the automatic routing will occur.The uri and name of the method will be used to find
     * the proper class/method.<br/> ie "POST" user/:uid will look for the User::post method</li>
     * <li>if set as an anonymous function the internal controller will be used.</li>
     * <li>if set to only a Controller, the $httpMethod will be used to find the method name (POST -> post, PUT -> put,
     * GET -> get, DELETE -> delete, PATCH ->patch)</li>
     * @param null|array $options
     * @throws Exceptions\Error\Exception500InternalServerError
     * @return $this
     */
    public function addRoute($httpMethods, $httpURI, $class = null, $options = null)
    {
        if( is_array($httpMethods) )
        {
            if($this->isControllerMethodDefined($class))
            {
                throw new \RestMe\Exceptions\Error\Exception500InternalServerError('There is a configuration problem for this URL');
            }

            foreach( $httpMethods as $httpMethod)
            {
                $route = new \RestMe\Route($httpMethod, $httpURI, $class, $options);
                $this->routes[$httpMethod][$httpURI] = $route;
            }
        }
        else
        {
            $route = new \RestMe\Route($httpMethods, $httpURI, $class, $options);
            $this->routes[$httpMethods][$httpURI] = $route;
        }

        return $this;
    }

    /**
     * Get the class for specified http method and http uri
     * @param string $httpMethod The http method of the resource to retrieve
     * @param string $httpURI The uri of the resource to retrieve
     * @return null|\RestMe\Route
     */
    public function findRoute($httpMethod, $httpURI)
    {
        if( isset($this->routes[$httpMethod]) )
        {
            $methodMaps = $this->routes[$httpMethod];
            foreach ($methodMaps as $routeURI => $route)
            {
                $parts = explode("/", $routeURI) ;
                $map = array() ;
                foreach($parts as $part)
                {
                    if (isset($part[0]) && $part[0] == ":" && $part[1] == "?")
                    {
                        $map[] = "?[^/]*";
                    }
                    else if (isset($part[0]) && $part[0] == ":")
                    {
                        $map[] = "[^/]+";
                    }
                    else
                    {
                        $map[] = $part;
                    }
                }

                if(preg_match("%^".implode("/", $map )."$%", $httpURI) )
                {
                    $this->route = $route;
                    return $route;
                }
            }
        }

        return null;
    }

    /**
     * @return \string Version of the API (v1, v2, v2.2, v2.2a, ...)
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return \string Name of the API
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return the currently accessed route
     * @return null|\RestMe\Route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Validate the api version, then run the Server to handle the request and prepare the response
     */
    public function run( \RestMe\Server $server)
    {
        $this->server = $server;

        $route = $this->findRoute($server->getRequest()->getMethod(), $server->getRequest()->getRouteURI() );
        if( is_null($route) )
        {
            throw new \RestMe\Exceptions\Error\Exception404NotFound("The requested resource doesn't exists");
        }

        //  check the mime type is handle by either the route, api or server or throw 415
        $askedMimeTypes = $server->getRequest()->acceptMime();
        $acceptedMimeTypes = array_merge( (array)$route->getAcceptedMimeTypes(), (array)$this->getAcceptedMimeTypes(), (array)$server->getAcceptedMimeTypes() );

        foreach( $askedMimeTypes as $type)
        {
            if ($type === "*/*")
            {
                $server->setReturnMimeType($server->getDefaultReturnMimeType());
                break;
            }

            if( in_array($type, $acceptedMimeTypes))
            {
                $server->setReturnMimeType($type);
                break;
            }
        }

        if( is_null($server->getReturnMimeType()) )
        {
            throw new \RestMe\Exceptions\Error\Exception415UnsupportedMediaType("The '".implode("', '",$askedMimeTypes)."' mime type(s) is (are) not supported by this resource. The supported mime type(s) is (are) '".implode("',' ", $acceptedMimeTypes)."'");
        }

        //  execute API pre modules
        $server->executeModules( $this->getPreModules() );

        //  execute the resource modules
        $server->executeModules( $route->getPreModules() );

        //  execute the controller
        $server->executeAction( $route->getClass() );

        // execute the resource post modules
        $server->executeModules( $route->getPostModules() );

        // execute the API post modules
        $server->executeModules( $this->getPostModules() );
    }

    private function isControllerMethodDefined($class = null)
    {
        if( is_string($class) )
        {
            $parts = explode("::",$class);
            if( count($parts) == 2 )
            {
                return true;
            }
        }
        return false;
    }
}
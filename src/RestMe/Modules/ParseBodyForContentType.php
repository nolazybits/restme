<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 17/10/14
 * Time: 10:59 AM
 */

namespace RestMe\Modules;

class ParseBodyForContentType
    implements \RestMe\Controller
{
    /**
     * Parse any put data depending on the content type received
     * @param \RestMe\Server $server
     * @return \RestMe\Action|\RestMe\View|\RestMe\Controller
     */
    function execute(\RestMe\Server $server)
    {
        $httpMethod = ucfirst(strtolower( $server->getRequest()->getMethod() ));
        $method = ucfirst(strtolower( $httpMethod ));
        $content_type = $server->getRequest()->getHeader('Content-Type');

        switch ($content_type)
        {
            case 'application/x-www-form-urlencoded':
                if( $httpMethod === \RestMe\Http\Methods::PUT )
                {
                    $parsed = \RestMe\Http\Parser\ParserFormUrlencoded::parse($server->getRequest()->getPut());
                    $server->getRequest()->setPut($parsed);
                }
            break;
            case 'application/json':
                $parsed = \RestMe\Http\Parser\ParserJSON::parse($server->getRequest()->getPut());
                $server->getRequest()->{"set".$method}(['payload'=>$parsed]);
            break;
        }
    }
}
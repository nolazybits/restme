<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 6/05/14
 * Time: 5:01 PM
 */

namespace RestMe;

/**
 * Interface describe a View for rendering an Response
 */
interface View
    extends Action
{
    /**
     * Render this view
     * @param \RestMe\Server $server
     * @return \RestMe\Server
     */
    function execute(\RestMe\Server $server) ;
}
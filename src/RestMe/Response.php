<?php
/**
 * User: nolazybits
 * Date: 6/05/14
 * Time: 4:48 PM
 */

namespace RestMe;

/**
 * Hold the response in a Server
*/
class Response
{

    /**
     * @var \RestMe\Server
     */
    private $server;
    /**
     * @var \string[]
     */
    private $headers;
    /**
     * @var \mixed
     */
    private $data;
    /**
     * @var \string status code
     */
    private $statusCode;

    /**
     * @param \RestMe\Server $server
     */
    public function __contruct(\RestMe\Server $server=null)
    {
        $this->server = $server;
        $this->statusCode = \RestMe\Http\StatusCodes::SUCCESS_OK;
    }

    /**
     * Adds a header to the response
     * @param \string $header
     * @return \RestMe\Response
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
        return $this  ;
    }

    /**
     * Clean the headers set on the response
     * @return \RestMe\Response
     */
    public function cleanHeader()
    {
        $this->headers = Array();
        return $this ;
    }

    /**
     * Show the headers
     * @return \RestMe\Response
     */
    public function showHeader()
    {
        if(count($this->headers) >=1)
        {
            foreach($this->headers as $value)
            {
                header($value);
            }
        }
        return $this ;
    }

    /**
     * Check if headers were sent
     * @return \bool
     */
    public function headerSent()
    {
        return headers_sent();
    }

    /**
     * TODO: how this integrate with renderers
     * Sends the partial response already, skip buffering, good for big responses
     * @param mixed $response
     * @return \RestMe\Response
     */
    public function send($response)
    {
        if(!$this->headerSent())
        {
            $this->showHeader();
        }
        echo $response ;
        return $this;
    }

    /**
     * Set the response
     * @param mixed $data
     * @return \RestMe\Response
     */
    public function setData($data)
    {
        $this->data = $data ;
        return $this ;
    }


    /**
     * Return the data set
     * @return \mixed;
     */
    public function getData()
    {
        return $this->data ;
    }

    /**
     * Set the response to null
     * @return \RestMe\Response
     */
    public function cleanData()
    {
        $this->data = null ;
        return $this ;
    }

    /**
     * Set the response status code
     * @param \string $statusCode
     * @return \RestMe\Response
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode ;
        return $this ;
    }


    /**
     * Return the data set
     * @return \mixed;
     */
    public function getStatusCode()
    {
        return $this->statusCode ;
    }

    /**
     * Set the response to null
     * @return \RestMe\Response
     */
    public function cleanStatusCode()
    {
        $this->statusCode = null ;
        return $this ;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 6/05/14
 * Time: 5:00 PM
 */

namespace RestMe;

/**
 * Describe a possible Controller to handle a Request
 */
interface Controller
    extends Action
{

    /**
     * Execute the Default action of this controller
     * @param Server $server
     * @return \RestMe\Action|\RestMe\View|\RestMe\Controller
     */
    function execute(Server $server) ;

}

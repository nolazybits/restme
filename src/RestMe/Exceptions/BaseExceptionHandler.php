<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 4/05/14
 * Time: 11:41 PM
 */

namespace RestMe\Exceptions;

/**
 *
 */
class BaseExceptionHandler
    implements \RestMe\Exceptions\IExceptionHandler
{
    /**
     * @var \RestMe\Server
     */
    protected $server;

    /**
     * @param \RestMe\Server $server
     */
    public function __construct($server)
    {
        $this->server = $server;
    }

    public function register()
    {
        set_exception_handler( array ( $this,'exceptionHandler' ) );
        set_error_handler( array( $this,'errorHandler' ) );
        register_shutdown_function(array ($this, 'shutDownHandler'));
    }

    /**
     * @param $exception
     */
    public function exceptionHandler ( $exception )
    {

    }

    /**
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     */
    public function errorHandler ( $errno, $errstr, $errfile, $errline )
    {

    }

    /**
     *
     */
    public function shutDownHandler()
    {

    }

    /**/
}
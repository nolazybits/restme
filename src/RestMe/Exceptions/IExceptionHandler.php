<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 4/05/14
 * Time: 11:42 PM
 */

namespace RestMe\Exceptions;

/**
 *
 */
interface IExceptionHandler
{
    function exceptionHandler( $exception );
    function errorHandler($errno, $errstr, $errfile, $errline);
    function shutDownHandler ();
}
<?php
namespace RestMe\Exceptions;
/**
 *
 */
class RestException
    extends \Exception
{
    /**
     * @var mixed
     */
    public $data;

    /**
     * @var int
     */
    public $message_code;
    public $priority;

    /**
     * @param int $http_code
     * @param int $message_code
     * @param string $message
     * @param mixed $data
     * @param \Exception $previous
     */
    public function __construct($http_code, $message_code, $message, $data = null, $previous = null)
    {
        $this->previousException = $previous;

        // https status code
        $this->code = $http_code;

        // properties specific to this error
        $this->message_code = $message_code;
        $this->message = $message;
        $this->data = $data;
    }
}

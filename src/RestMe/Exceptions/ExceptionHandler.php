<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 4/05/14
 * Time: 11:40 PM
 */

namespace RestMe\Exceptions;
/**
 *
 */
class ExceptionHandler
    extends \RestMe\Exceptions\BaseExceptionHandler
{
    /**
     * @param \RestMe\Server $server
     */
    public function __construct( $server )
    {
        parent::__construct( $server );
    }

    /**
     * @param \Exception $exception
     * @return \RestMe\Generics\View|void
     */
    public function exceptionHandler ( $exception )
    {
        $output['errors'] = array();
        $error = new \stdClass();
        $error->message = $exception->getMessage();

        // put other public values if set, exclude the previous exception and debug messaging
        foreach($exception as $key => $value)
        {
            if ($key == "xdebug_message")
            {
                continue;
            }

            if (!is_null($value))
            {
                $error->$key = $value;
            }
        }

        array_push($output['errors'], $error );

        if (defined("REST_DEBUGGING") &&  REST_DEBUGGING === true)
        {
            $debugging = ob_get_contents();
            if (!empty($debugging))
            {
                $errors = explode("\n", $debugging);
                if (count($errors) > 1)
                {
                    for ($i = 0; $i < count($errors); $i++)
                    {
                        $error = new \stdClass();
                        $error->debug = $errors[$i];
                        if (!empty($error->debug))
                        {
                            array_push($output['errors'], $error );
                        }
                    }
                }
                else
                {
                    $error = new \stdClass();
                    $error->message = $debugging;
                    array_push($output['errors'], $error );
                }
            }
        }

        $response = new \RestMe\ResponseData();
        $response->message = "Error";
        $response->data = $output;

        $this->server->getResponse()->setStatusCode($exception->getCode());
        $this->server->getResponse()->setData($response);
        $view = new \RestMe\Generics\View();
        $view->execute($this->server);

        //echo $this->server->getResponse()->getData();die;

        $this->server->echoResponse();
   }

    /**
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     */
    public function errorHandler ( $errno, $errstr, $errfile, $errline )
    {
        echo "$errno, $errstr, $errfile, $errline";
    }

    /**
     *
     */
    public function shutDownHandler()
    {
        $lasterror = error_get_last();

        if( isset($lasterror) && isset($lasterror['type']))
        {
            switch ($lasterror['type'])
            {
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                case E_RECOVERABLE_ERROR:
                case E_CORE_WARNING:
                case E_COMPILE_WARNING:
                case E_PARSE:
                    $error = "[SHUTDOWN] lvl:" . $lasterror['type'] . " | msg:" . $lasterror['message'] . " | file:" . $lasterror['file'] . " | ln:" . $lasterror['line'];

            }
        }
    }

    /**/
}

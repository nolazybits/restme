<?php
/**
 * User: nolazybits
 * Date: 9/04/14
 * Time: 3:19 PM
 */

namespace RestMe;

/**
 * Interface Action
 * Inteface for a possible action for to be taken by Rest\Server
 * @package RestMe
 */
interface Action {}
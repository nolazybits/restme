<?php
/**
 * User: nolazybits
 * Date: 9/04/14
 * Time: 12:24 PM
 */

namespace RestMe;


abstract class Options
{
    /**
     * Contains the string mimeTypes <br/>
     * Default is array('application/json')
     * @var \array
     */
    protected $mimeTypes;

    /**
     * Contains the class name and method for the module to be executed before the main controller
     * @var \array
     */
    protected $pre_modules;

    /**
     * Contains the class name and method for the module to be executed after the main controller
     * @var \array
     */
    protected $post_modules;

    public function __construct()
    {
        $this->mimeTypes = array();
        $this->pre_modules = array();
        $this->post_modules = array();
    }

    /**
     * Add a mime type the server is handling
     * @param \string $mimeType
     */
    public function addAcceptedMimeType($mimeType)
    {
        $this->mimeTypes[$mimeType] = true;
    }

    /**
     * Remove a mime type the server is handling
     * @param \string $mimeType
     */
    public function removeAcceptedMimeType($mimeType)
    {
        unset($this->mimeTypes[$mimeType]);
    }

    /**
     * Check if the mime type is handle by the server
     * @param string $mimeType
     * @return bool
     */
    public function acceptMimeType($mimeType)
    {
        return array_key_exists($mimeType, $this->mimeTypes);
    }

    /**
     * Add multiple mime types at once.<br />
     * The array to pass is a indexed array.
     * @param array $mimeTypes
     */
    public function addAcceptedMimeTypes($mimeTypes)
    {
        foreach( $mimeTypes as $extension )
        {
            $this->mimeTypes[$extension] = true;
        }
    }

    /**
     * Remove all the mime types
     */
    public function clearAcceptedMimeTypes()
    {
        $this->mimeTypes = array();
    }

    /**
     * Returns an indexed array (sorting not important) of mime types handled
     * @return array
     */
    public function getAcceptedMimeTypes()
    {
        return array_keys($this->mimeTypes);
    }

    /**
     * Add a pre module
     * @param string $module
     */
    public function addPreModule($module)
    {
        $this->pre_modules[$module] = true;
    }

    /**
     * Remove a pre module
     * @param string $module
     */
    public function removePreModule($module)
    {
        unset($this->pre_modules[$module]);
    }

    /**
     * Check if the mimeType is handle by the server
     * @param string $module
     * @return bool
     */
    public function hasPreModule($module)
    {
        return array_key_exists($module, $this->pre_modules);
    }

    /**
     * Add multiple mimeTypes at once.<br />
     * The array to pass is a indexed array.
     * @param array $modules
     */
    public function addPreModules($modules)
    {
        foreach( $modules as $module )
        {
            $this->pre_modules[$module] = true;
        }
    }

    /**
     * Remove all the mimeTypes from the server
     */
    public function clearPreModules()
    {
        $this->pre_modules = array();
    }

    /**
     * Returns an indexed array (sorting not important) of mimeTypes handle by the server
     * @return array
     */
    public function getPreModules()
    {
        if( !is_null($this->pre_modules))
        {
            return array_keys($this->pre_modules);
        }

    }
    /**
     * Add a post module
     * @param string $module
     */
    public function addPostModule($module)
    {
        $this->post_modules[$module] = true;
    }

    /**
     * Remove a post module
     * @param string $module
     */
    public function removePostModule($module)
    {
        unset($this->post_modules[$module]);
    }

    /**
     * Check if a modules is in the post modules
     * @param string $module
     * @return bool
     */
    public function hasPostModule($module)
    {
        return array_key_exists($module, $this->post_modules);
    }

    /**
     * Add multiple post modules at once.<br />
     * @param array $modules
     */
    public function addPostModules($modules)
    {
        foreach( $modules as $module )
        {
            $this->post_modules[$module] = true;
        }
    }

    /**
     * Remove all the post modules
     */
    public function clearPostModules()
    {
        $this->post_modules = array();
    }

    /**
     * Returns an indexed array (sorting not important) of post modules
     * @return array
     */
    public function getPostModules()
    {
        return array_keys($this->post_modules);
    }
} 
<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 21/05/14
 * Time: 11:41 AM
 */

namespace RestMe\Generics;

class View
    implements \RestMe\View
{
    /**
     * @var \stdClass
     */
    protected $body;

    /**
     * Render this view
     * @param \RestMe\Server $server
     * @throws \RestMe\Exceptions\Error\Exception415UnsupportedMediaType
     * @return \RestMe\Server
     */
    public function execute(\RestMe\Server $server)
    {
        // get the result value object to output
        $data = $server->getResponse()->getData();
        if( !isset($data) )
        {
            $data = new \RestMe\ResponseData();
            $server->getResponse()->setData( $data );
        }

        // output the response header
        header(\RestMe\Http\HeaderConstants::HTTP_VERSION_1_1 . $server->getResponse()->getStatusCode());

        if (!is_null($data)) {
            //  do not cache results
            $response = $server->getResponse();
            $response->addHeader(\RestMe\Http\HeaderConstants::CONTENT_NO_CACHE);

            //  find the requested type
            $mimeType = $server->getReturnMimeType();

            //  now call renderers per mime types, they will populate the response object
            switch ($mimeType) {
                case 'application/json':
                    $renderer = new \RestMe\Generics\Views\Renderers\JSON();
                    $renderer->execute($server);
                break;

                default:
                    $renderer = new \RestMe\Generics\Views\Renderers\HTML();
                    $renderer->execute($server);
//                    throw new \RestMe\Exceptions\Error\Exception415UnsupportedMediaType("We haven't implemented yet a parser for the '" . $mimeType . "' type");
                break;
            }
        }

        return $server;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 21/05/14
 * Time: 11:41 AM
 */

namespace RestMe\Generics\Views\Renderers;

class JSON
    implements \RestMe\View
{
    /**
     * Render as JSON
     * @param \RestMe\Server $server
     * @throws \RestMe\Exceptions\Error\Exception500InternalServerError
     * @return \RestMe\Server
     */
    function execute(\RestMe\Server $server)
    {
        $response = $server->getResponse();
        $response->addHeader(\RestMe\Http\HeaderConstants::CONTENT_TYPE_JSON.\RestMe\Http\HeaderConstants::CHARSET_UTF8);

        $encoded_response = json_encode($response->getData());

        if ($encoded_response === FALSE)
        {
            throw new \RestMe\Exceptions\Error\Exception500InternalServerError("Couldn't create json object.");
        }

        /*$opt_pretty = $server->getParameter(\RestMe\Modules\OptionPretty::MODULE_OPT_PRETTY);
        if ( isset($opt_pretty) )
        {
            $encoded_response = $this->indent($encoded_response);
        }*/
        $response->setData($encoded_response);
        return $server;
    }

    /**
     * Indents a flat JSON string to make it more human-readable.
     * @param string $json  The original JSON string to process.
     * @return string  Indented version of the original JSON string.
     */
    private function indent($json)
    {
        $result      = '';
        $pos         = 0;
        $strLen      = strlen($json);
        $indentStr   = '  ';
        $newLine     = "\n";
        $prevChar    = '';
        $outOfQuotes = true;

        for ($i=0; $i<=$strLen; $i++)
        {
            // Grab the next character in the string.
            $char = substr($json, $i, 1);

            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\')
            {
                $outOfQuotes = !$outOfQuotes;

                // If this character is the end of an element,
                // output a new line and indent the next line.
            } else if(($char == '}' || $char == ']') && $outOfQuotes)
            {
                $result .= $newLine;
                $pos --;
                for ($j=0; $j<$pos; $j++)
                {
                    $result .= $indentStr;
                }
            }

            // Add the character to the result string.
            $result .= $char;

            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes)
            {
                $result .= $newLine;
                if ($char == '{' || $char == '[')
                {
                    $pos ++;
                }

                for ($j = 0; $j < $pos; $j++)
                {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }

        return $result;
    }
}
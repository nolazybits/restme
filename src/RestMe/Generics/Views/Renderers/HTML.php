<?php
/**
 * Created by IntelliJ IDEA.
 * User: nolazybits
 * Date: 21/05/14
 * Time: 11:41 AM
 */

namespace RestMe\Generics\Views\Renderers;

class HTML
    implements \RestMe\View
{
    /**
     * Render as JSON
     * @param \RestMe\Server $server
     * @throws \RestMe\Exceptions\Error\Exception500InternalServerError
     * @return \RestMe\Server
     */
    function execute(\RestMe\Server $server)
    {
        $response = $server->getResponse();
        $response->addHeader(\RestMe\Http\HeaderConstants::CONTENT_TYPE_HTML.\RestMe\Http\HeaderConstants::CHARSET_UTF8);

        $data = $response->getData();

        if (is_object($data))
        {
            $formattedResponse = "<pre>". print_r($data, true) ."</pre>";
        }
        else
        {
            $formattedResponse = "<p>".$data."</p>";
        }

        $response->setData($formattedResponse);
        return $server;
    }
}
<?php
/**
 * User: nolazybits
 * Date: 9/04/14
 * Time: 3:21 PM
 */

namespace RestMe\Generics;


class Action
implements \RestMe\Action
{
    private $code;

    /**
     * @param null $code
     */
    public function __construct($code = null)
    {
        $this->code = $code;
    }

    /**
     * @param \RestMe\Server $server
     * @return mixed
     */
    public function execute(\RestMe\Server $server)
    {
        return call_user_func($this->code, $server);
    }
} 